using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ScreenManager : MonoBehaviour
{
    [SerializeField] Text textTitle;
    [SerializeField] Text textTitle2;
    [SerializeField] Image bossLaught;
    [SerializeField] Image bossDie;
    float cooldown = 6;

    private void Update()
    {
        if (cooldown > 0)
        {
            cooldown -= Time.deltaTime;

        }
        else if (cooldown < 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }

    public void SetUp() {
        gameObject.SetActive(true);
        if (GameManager.Instance.bossFightWin)
        {
            bossDie.gameObject.SetActive(true);
            bossLaught.gameObject.SetActive(false);
            textTitle.text = "YOU WIN";
            textTitle2.text = "YOU WIN";
        }
        else
        {
            bossDie.gameObject.SetActive(false);
            bossLaught.gameObject.SetActive(true);
            textTitle.text = "YOU LOSE";
            textTitle2.text = "YOU LOSE";
        }
    }

    public void SetDisable()
    {
        gameObject.SetActive(false);
    }


}
