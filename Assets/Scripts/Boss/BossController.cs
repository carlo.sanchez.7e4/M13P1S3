using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class BossController : EnemyControler
{
    private Wilberforce.Colorblind colorblind;
    public int typeFilter;
    public float speed;

    public Animator bossAnimator;
    
    bool movex = false;
    bool movement = false;
    bool summon = true;
    GameObject enemy;
    Camera mainCamera;

    public HealthBar healthBar;
    

    //Fire mechanics
    public float maxColdown;
    private float coldown;
    private bool readyToFire;
    [SerializeField] private float rotationSpeed = 3.0f;
    [SerializeField] private float bulletSpeed;    
    [SerializeField] private Rigidbody2D bullet;
    [SerializeField] private Transform aimPoint;

    private Vector3 m_lastKnownPosition = Vector3.zero;
    private Quaternion m_lookAtRotation;
    private Vector3 angle;
    private Quaternion targetRotation;
    private Vector3 shootVector;
    private Transform m_target;

    //Spawn mechanics
    [SerializeField] private GameObject cultista;
    [SerializeField] private GameObject engendro;
    bool spawnCultista, spawnEngendro;
    

    // Start is called before the first frame update
    void Start()
    {
        colorblind = Camera.main.GetComponent<Wilberforce.Colorblind>();
        coldown = maxColdown;
        m_target = GameObject.Find("Player").GetComponent<Transform>();
        enemy = cultista;
        mainCamera = Camera.main;

        healthBar.SetMaxHealth(enemyHealth);
    }

    // Update is called once per frame
    void Update()
    {
        if (coldown > 0)
        {
            coldown -= Time.deltaTime;

        }
        else if (coldown < 0)
        {
            readyToFire = true;
        }
        
        if (GameManager.Instance.bossFightWin) {
            FaseBoss();
        }

        healthBar.SetHealth(enemyHealth);

        if (enemyHealth <= 0)
        {
            bossAnimator.Play("Death");
        }
    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        //enemyHealth--;
        if (collision.gameObject.CompareTag("MainCamera")) {
            
            if (movex) {
                movex = false;
                Debug.Log(movex);
            }
            else
            {
                movex = true;
                Debug.Log(movex);
            }
        }
        //enemyHealth--;
        healthBar.SetHealth(enemyHealth);

        FaseBoss();
    }


    void FaseBoss() {
        switch (enemyHealth)
        {
            case int i when i > 30 && i < 40:
                //FASE 1
                BossAttack(true, 2f, cultista);
                typeFilter = 0;
                break;

            case int i when i > 20 && i < 30:
                //FASE 2
                MovementEnemy();
                BossAttack(false, 1.3f, cultista);
                typeFilter = 1;
                break;

            case int i when i > 10 && i < 20:
                //FASE 3
                transform.position = mainCamera.transform.position + new Vector3(0,0,+11);
                BossAttack(true, 1.2f, engendro);
                typeFilter = 2;
                break;

            case int i when i > 10 && i < 0:
                //FASE 
                if (summon) {
                    BossAttack(true, 0.9f, cultista);
                    summon = false;
                    
                }
                else {
                    BossAttack(true, 0.9f, engendro);
                    summon = true;
                }
                typeFilter = 3;
                break;
            case int i when i < 0 || i == 0:
                Destroy(this.gameObject);
                Destroy(GameObject.Find("Player"));
                GameManager.Instance.bossFightWin = true;
                GameManager.Instance.SaveScore();
                GameManager.Instance.screenManager.SetUp();
                break;
            default:
                BossAttack(true, 1f, cultista);
                break;
        }
        colorblind.Type = typeFilter;
    }

    void MovementEnemy()
    {
        if (movex) {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }
        else {
            transform.position += Vector3.left * speed * Time.deltaTime;
        }
    }

    void BossAttack(bool isSpawning, float shootCd, GameObject enemy) {

        if (readyToFire) {
            bossAnimator.Play("Attack");
            bossAnimator.Play("Idle");
            readyToFire = false;
            
            //Traccking system
            m_lastKnownPosition = m_target.transform.position;

            shootVector = m_lastKnownPosition - transform.position;
            angle = Quaternion.Euler(0, 0, 180) * shootVector;
            targetRotation = Quaternion.LookRotation(forward: Vector3.forward, upwards: angle);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
            Shoot();

            if (isSpawning)
            {
                SpawnEnemies(enemy);
            }

            coldown = shootCd;
        }
    }

    void SpawnEnemies(GameObject enemy) {       
            // probability to spawn an enemy on top of a platform
            float random = Random.Range(0f, 10f);

            if (random >= 0 && random <= 2)
            {
            bossAnimator.Play("Summon");
            bossAnimator.Play("Idle");
            //obj = Instantiate(obj, new Vector2(lenghtPlatform, height), Quaternion.identity)
            //Instantiate(enemy, transform.position - new Vector3(0, -2, 0), Quaternion.identity);
            Instantiate(enemy, transform.position - new Vector3(0, +1, 0), Quaternion.identity);

            
        }               
    }

    void Shoot()
    {
        Rigidbody2D instance = Instantiate(bullet, (aimPoint.position), targetRotation * Quaternion.Euler(0, 0, -90));
        shootVector.Normalize();
        instance.velocity = shootVector * bulletSpeed;
        

    }

    public void TakeDmg(int damage)
    {
        damage--;
        if (!bossAnimator.GetCurrentAnimatorStateInfo(0).IsName("Hit"))
        {
            bossAnimator.Play("Hit");
            bossAnimator.Play("Idle");
        }
    }
}
