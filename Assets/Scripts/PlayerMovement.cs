using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    private float Speed = 0;
    Vector3 lastPosition;
    float odometerDistance;

    public ParticleSystem dust;

    [SerializeField] public float maxSpeed;
    [SerializeField] private float Acceleration;
    [SerializeField] private float jumpStrengh;
    [SerializeField] private Animator animator;    
    [SerializeField] private float DeploymentHeight;

    private bool onGround;
    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        animator = this.GetComponent<Animator>();

        if (PlayerPrefs.GetInt("JumpBoost") == 1)
        {
            jumpStrengh += 300;

        }


    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Space) && onGround)
        {
            //Debug.Log("IM JUMPING");
            //CreateDust();
            onGround = false;
            rb.AddForce(transform.up * jumpStrengh);
            
        }

        if (!onGround)
        {
            animator.SetBool("Jumping", true);
        }
        else
        {
            animator.SetBool("Jumping", false);
        }

        movement();
        meterCounter();

        
        
    }

    void CheckGroundStatus()
    {

        RaycastHit hit;
        Ray landingRay = new Ray(transform.position, Vector3.down);
        Debug.DrawRay(transform.position, Vector3.down * jumpStrengh);

        if (Physics.Raycast(landingRay, out hit, jumpStrengh))
        {
            if (hit.collider == null)
            {
                onGround = false;
                Debug.Log(onGround);
            }
            else
            {
                onGround = true;
                Debug.Log(onGround);
            }

        }
    }

    void meterCounter() {
        Vector3 currentPosition = transform.position;   
        float distance = Vector3.Distance(currentPosition, lastPosition);   
        odometerDistance += distance;       
        lastPosition = currentPosition;
        GameManager.Instance.meters = odometerDistance;
    }

    void movement() {

        if (Speed < maxSpeed)
        {
            Speed = Speed - Acceleration * Time.deltaTime;
            transform.Translate(Vector3.left * Time.deltaTime * Speed);
            //Debug.Log("Speed without max"+Speed);

        }
        else {

            Speed = maxSpeed;
            transform.Translate(Vector3.left * Time.deltaTime * Speed);
            //Debug.Log("Speed wit max" + Speed);
        }

        
    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Ground"))
        {
            onGround = true;
        }

        if (collision.gameObject.CompareTag("MainCamera"))
        {
            GameManager.Instance.bossFightWin = false;
            GameManager.Instance.SaveScore();
            Destroy(gameObject);
            GameManager.Instance.screenManager.SetUp();
            //SceneManager.LoadScene("GameOver");
        }

        /*
        if (collision.gameObject.tag.Equals("Heart"))
        {
            if(GameManager.Instance.playerHealth < 4 && GameManager.Instance.playerHealth > 0)
            {
                GameManager.Instance.playerHealth++;
            }
            
        }
        */
    }

    private void OnCollisionExit2D(Collision2D collision2D)
    {
        if (collision2D.gameObject.tag.Equals("Ground"))
        {
            onGround = false;
        }
    }

    /*
    private void CreateDust() {
        dust.Play();
    }
    */

    
}
