using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShopEvents : MonoBehaviour
{
    public Text textVials;

    private void Update()
    {
        Debug.Log("TOTAL VIALS: " + PlayerPrefs.GetInt("TotalVials"));
        int totalVials;
        if (PlayerPrefs.GetInt("TotalVials") == null)
        {
            PlayerPrefs.SetInt("TotalVials", 0);
        }
        else {
            totalVials = PlayerPrefs.GetInt("TotalVials");
            textVials.text = "Total Vials: " + totalVials.ToString();
        }
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
