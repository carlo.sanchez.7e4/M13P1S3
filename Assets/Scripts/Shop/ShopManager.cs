using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    public Button[] items;
    public int[] prices;
    public int index;
    private int vialsBeforeBuy;
    [SerializeField] private TextMeshProUGUI textOne;
    [SerializeField] private TextMeshProUGUI textTwo;
    [SerializeField] private TextMeshProUGUI textTree;
    [SerializeField] private TextMeshProUGUI textfour;

    // Start is called before the first frame update
    void Start()
    {
        index = 0;
        items[index].GetComponent<Image>().enabled = true;

        if (!PlayerPrefs.HasKey("healthUp")) {
            PlayerPrefs.SetInt("healthUp", 0);

        } else if (!PlayerPrefs.HasKey("DamageUp")) {
            PlayerPrefs.SetInt("DamageUp", 0);

        } else if (!PlayerPrefs.HasKey("VialMultiplier")) {
            PlayerPrefs.SetInt("VialMultiplier", 0);

        }
        else if (!PlayerPrefs.HasKey("JumpBoost"))
        {
            PlayerPrefs.SetInt("JumpBoost", 0);

        }



    }

    // Update is called once per frame
    void Update()
    {
        SelectItem();
        BuyItem();

        if (PlayerPrefs.GetInt("healthUp") == 1)
        {

            textOne.color = new Color(0, 128, 0, 1);

        }

        if (PlayerPrefs.GetInt("DamageUp") == 1)
        {

            textTwo.color = new Color(0, 128, 0, 1);

        }

        if (PlayerPrefs.GetInt("VialMultiplier") == 1)
        {

            textTree.color = new Color(0, 128, 0, 1);

        }

        if (PlayerPrefs.GetInt("JumpBoost") == 1)
        {

            textfour.color = new Color(0, 128, 0, 1);

        }

       




    }

    private void SelectItem()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            MoveCursorDown();
        } else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            MoveCursorUp();
        }
    }

    private void MoveCursorDown()
    {
        items[index].GetComponent<Image>().enabled = false;
        index++;
        if (index >= items.Length)
        {
            index = 0;
        }
        items[index].GetComponent<Image>().enabled = true;
    }

    private void MoveCursorUp()
    {
        items[index].GetComponent<Image>().enabled = false;
        index--;
        if (index < 0)
        {
            index = items.Length - 1;
        }
        items[index].GetComponent<Image>().enabled = true;
    }

    private void BuyItem()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (index == 0)
            {
                if ((PlayerPrefs.GetInt("TotalVials") > prices[index]) )
                {
                    if (PlayerPrefs.GetInt("healthUp") != 1)
                    {
                        vialsBeforeBuy = PlayerPrefs.GetInt("TotalVials");

                        PlayerPrefs.SetInt("TotalVials", vialsBeforeBuy - prices[index]);

                        PlayerPrefs.SetInt("healthUp", 1);

                        Debug.Log("Player buys healthUp " + PlayerPrefs.GetInt("healthUp"));
                        Debug.Log("Vials after buy " + PlayerPrefs.GetInt("TotalVials"));

                    }
                    else {
                        Debug.Log("Player already have the item");
                    
                    }
                }
                else {

                    Debug.Log("The player dosn't have enought money");

                }
            }
            else if (index == 1)
            {
                if ((PlayerPrefs.GetInt("TotalVials") > prices[index]))
                {
                    if (PlayerPrefs.GetInt("DamageUp") != 1)
                    {

                        vialsBeforeBuy = PlayerPrefs.GetInt("TotalVials");

                        PlayerPrefs.SetInt("TotalVials", vialsBeforeBuy - prices[index]);

                        PlayerPrefs.SetInt("DamageUp", 1);

                        Debug.Log("Player buys DamageUp " + PlayerPrefs.GetInt("DamageUp"));
                        Debug.Log("Vials after buy " + PlayerPrefs.GetInt("TotalVials"));
                    }
                    else
                    {
                        Debug.Log("Player already have the item");

                    }
                    
                }
                else
                {

                    Debug.Log("The player dosn't have enought money");

                }

            }
            else if (index == 2)
            {
                if ((PlayerPrefs.GetInt("TotalVials") > prices[index]))
                {

                    if (PlayerPrefs.GetInt("VialMultiplier") != 1)
                    {

                        vialsBeforeBuy = PlayerPrefs.GetInt("TotalVials");

                        PlayerPrefs.SetInt("TotalVials", vialsBeforeBuy - prices[index]);

                        PlayerPrefs.SetInt("VialMultiplier", 1);

                        Debug.Log("Player buys upgrade 3 " + PlayerPrefs.GetInt("VialMultiplier"));
                        Debug.Log("Vials after buy " + PlayerPrefs.GetInt("TotalVials"));
                    }
                    else
                    {
                        Debug.Log("Player already have the item");

                    }
                    
                }
                else
                {

                    Debug.Log("The player dosn't have enought money");

                }
            }
            else if (index == 3) {

                if ((PlayerPrefs.GetInt("TotalVials") > prices[index]))
                {
                    if (PlayerPrefs.GetInt("JumpBoost") != 1)
                    {

                        vialsBeforeBuy = PlayerPrefs.GetInt("TotalVials");

                        PlayerPrefs.SetInt("TotalVials", vialsBeforeBuy - prices[index]);

                        PlayerPrefs.SetInt("JumpBoost", 1);

                        Debug.Log("Player buys upgrade 4 " + PlayerPrefs.GetInt("JumpBoost"));
                        Debug.Log("Vials after buy " + PlayerPrefs.GetInt("TotalVials"));

                    }
                    else
                    {
                        Debug.Log("Player already have the item");

                    }

                    
                }
                else
                {

                    Debug.Log("The player dosn't have enought money");

                }
            }

        }
    }
}
