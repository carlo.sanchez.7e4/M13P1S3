using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private GameObject enemy;
    private int damage = 1;

    void Start() {
        if (PlayerPrefs.GetInt("DamageUp") == 1)
        {
            damage++;

        }
    }

    void OnBecameInvisible() {
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            enemy = collision.gameObject;
            enemy.GetComponent<EnemyControler>().enemyHealth -= damage;
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            enemy = collision.gameObject;
            enemy.GetComponent<EnemyControler>().enemyHealth -= damage;
            Destroy(gameObject);
        }
        if (collision.gameObject.tag.Equals("Boss"))
        {

            enemy = collision.gameObject;
            if (enemy.GetComponent<EnemyControler>().enemyHealth > 0)
            {
                enemy.GetComponent<BossController>().TakeDmg(damage);
            }
            Destroy(gameObject);

        } else if (!collision.gameObject.tag.Equals("Player")) {
            Destroy(gameObject);
        }

    }
}
