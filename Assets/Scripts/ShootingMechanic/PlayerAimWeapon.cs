using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAimWeapon : MonoBehaviour
{
    public Vector3 mouse_pos;
    public Vector3 mouse_pos_pixels;  
    private  Vector3 firePoint;
    private float angle;
    private Rigidbody2D rbBullet;
    [SerializeField] private Camera cam;
    [SerializeField] private Rigidbody2D bullet;

    void Start() {
        rbBullet = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) {


            mouse_pos_pixels = Input.mousePosition;
            mouse_pos = cam.ScreenToWorldPoint(mouse_pos_pixels);

            firePoint.x = transform.position.x;
            firePoint.y = transform.position.y;
            firePoint.z = 2;

            Vector2 shootVector = mouse_pos - firePoint;

            angle = Mathf.Atan2(shootVector.y, shootVector.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler( 0, 0, angle);

            rbBullet  = Instantiate(bullet, firePoint, transform.rotation);
            shootVector.Normalize();
            rbBullet.velocity = shootVector * 5;
            

        }
    }
}

