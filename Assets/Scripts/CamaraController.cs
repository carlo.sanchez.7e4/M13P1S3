using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraController : MonoBehaviour
{
    private PlayerMovement pm;
    public float panSpeed;

    private void Start()
    {
       // pm = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
       // panSpeed = Math.Abs(pm.maxSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;
        pos.x +=  Time.deltaTime*panSpeed;
        transform.position = pos;
    }
}
