using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonPlay : MonoBehaviour
{
    public Button buttonMute;
    public Button buttonExit;
    public Sprite muted;
    public Sprite sound;
    public bool isMute;

    public void ChangeScene(string a ) {
        Debug.Log("!");
        SceneManager.LoadScene(a);
    }      

    public void Mute()
    {
        isMute = !isMute;
        AudioListener.volume = isMute ? 0 : 1;

        if (isMute)
        {
            buttonMute.image.sprite = muted;
        }
        else
        {
            buttonMute.image.sprite = sound;
        }
    }
    
    public void Shop()
    {
        ChangeScene("ShopScreen");
    }

    public void Exit() {
        Application.Quit();
    }
}
