using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManager : MonoBehaviour
{
    public enum heartNumber
    {
        heart1,
        heart2,
        heart3,
        heart4
    }

    public heartNumber selector;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        switch (selector)
        {
            case heartNumber.heart4:
                if (GameManager.Instance.playerHealth < 4)
                {
                    gameObject.SetActive(false);
                }
                else
                {
                    gameObject.SetActive(true);
                }
                break;
            case heartNumber.heart3:
                if (GameManager.Instance.playerHealth < 3)
                {
                    gameObject.SetActive(false);
                }
                else {
                    gameObject.SetActive(true);
                }
                break;

            case heartNumber.heart2:
                if (GameManager.Instance.playerHealth < 2)
                {
                    gameObject.SetActive(false);
                }
                else
                {
                    gameObject.SetActive(true);
                }
                break;

            case heartNumber.heart1:
                if (GameManager.Instance.playerHealth < 1)
                {
                    gameObject.SetActive(false);
                }
                else
                {
                    gameObject.SetActive(true);
                }
                break;
        }
    }
}
