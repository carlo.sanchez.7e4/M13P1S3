using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    [SerializeField] Text textScore;
    [SerializeField] Text textVials;

    void Start() {
        int maxVials = 0;
        textScore.text = "Score: " + System.Math.Truncate( GameManager.Instance.meters);

        if (PlayerPrefs.GetInt("CantVials") == 0)
        {
            PlayerPrefs.GetInt("CantVials", 0);
        }
        else {
            maxVials = PlayerPrefs.GetInt("CantVials");
            textVials.text = "Vials: " + maxVials.ToString();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene("GameScreen");
        } else if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
