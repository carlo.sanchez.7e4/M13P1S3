using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickUp : MonoBehaviour
{

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Vial")) {
            GameManager.Instance.currentVials++;
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.tag.Equals("Heart")) {
            GameManager.Instance.currentVials++;
            Destroy(collision.gameObject);
        }

    }
}
