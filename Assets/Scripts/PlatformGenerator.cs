using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlatformGenerator : MonoBehaviour
{
    [SerializeField] float height;
    [SerializeField] float minHeight, maxHeight;
    [SerializeField] int spikeSpawnHeight;
    [SerializeField] Grid brickWithGrass;
    [SerializeField] GameObject auxPlatformX;
    [SerializeField] GameObject cultist;
    [SerializeField] GameObject vial;
    

    void Start()
    {
        InvokeRepeating(("Generation"), 1, 1.4f);
    }

    float direction;
    void Generation()
    {
        direction = auxPlatformX.transform.position.x;
        height = Random.Range(minHeight, maxHeight);
        GenerateFlatPlatform();
    }

    void GenerateFlatPlatform()
    {
        for (int y = 0; y < height; y++)//This will help spawn a tile on the y axis
        {
            spawnObj(brickWithGrass, y);
        }
        if (height < spikeSpawnHeight)
        {
            spawnObj(brickWithGrass, height);

            // probability to spawn an enemy on top of a platform
            float random = Random.Range(0f, 9f);

            if (random >= 0 && random <= 3)
            {
                spawnEnemy(cultist, height);

            } else if (random >= 6 && random <= 8) {

                spawnEnemy(vial, height);
            }
        }
    }

    void spawnObj(Grid obj, float height)//What ever we spawn will be a child of our procedural generation gameObj
    {
        //obj = Instantiate(obj, new Vector2(lenghtPlatform, height), Quaternion.identity)
        obj = Instantiate(obj, new Vector2(direction, height), Quaternion.identity);

        obj.transform.parent = this.transform;
    }

    void spawnEnemy(GameObject obj, float height)//What ever we spawn will be a child of our procedural generation gameObj
    {
        //obj = Instantiate(obj, new Vector2(lenghtPlatform, height), Quaternion.identity)
        obj = Instantiate(obj, new Vector3(direction +1f, height+1f, 2), Quaternion.identity);

        obj.transform.parent = this.transform;
    }

    
}