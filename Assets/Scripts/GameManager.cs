using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private Text textScore;
    private Text textVials;
    private SpriteRenderer spritePlayer;
    public float meters;
    public ScreenManager screenManager;
    public int playerHealth;
    public int currentVials;
    public bool bossFightWin;
    int totalVials;
    public int startHealth;

    private static GameManager _instance;
        
        public static GameManager Instance
        {
            get
            {
                if(_instance == null)
                {
                Debug.LogError("Game Manager is NULL!");
                }

            return _instance;
            }        
        }

    private void Awake()
    {
        if (PlayerPrefs.GetInt("CantVials") == null)
        {
            PlayerPrefs.SetInt("CantVials", 0);
        }
        if (PlayerPrefs.GetInt("TotalVials") == null)
        {
            PlayerPrefs.SetInt("TotalVials", 0);
        }

        if (PlayerPrefs.GetInt("healthUp") == 1)
        {
            playerHealth = 4;
        }
        else {
            playerHealth = 3;
        }

        startHealth = playerHealth;

        screenManager.SetDisable();

        bossFightWin = true;

        /*
         if (_instance != null && _instance != this) {

             Destroy(this.gameObject);
         }
         _instance = this;
        */

        if (_instance == null)
        {
            // Set this instance as the instance reference.
            _instance = this;
        }
        else if (_instance != this)
        {
            // If the instance reference has already been set, and this is not the
            // the instance reference, destroy this game object.
            Destroy(gameObject);
        }

        //DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        PlayerPrefs.SetInt("CantVials", 0);
        currentVials = 0;

        if (GameObject.Find("Player") != null)
        {
            spritePlayer = GameObject.Find("Player").GetComponent<SpriteRenderer>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (textScore == null && textVials == null)
        {
            textScore = GameObject.Find("TextMeters").GetComponent<Text>();
            textVials = GameObject.Find("TextVials").GetComponent<Text>();
        }
        else
        {
            textScore.text = "Meters: " + (int)System.Math.Round(meters * 100f) / 100f;
            textVials.text = "Vials: " + currentVials;
        }
    }

    public void playerDamaged(int damage) {
        playerHealth = playerHealth - damage;

        if (GameObject.Find("Player") != null)
        {
            StartCoroutine(FlashRed());

        }

        if (playerHealth <= 0) {
            bossFightWin = false;
            SaveScore();
            Destroy(GameObject.Find("Player"));
            screenManager.SetUp();
            //Application.LoadLevel(Application.loadedLevel);
            // SceneManager.LoadScene("GameOver");
        }
    }

    public void SaveScore() {
        totalVials = PlayerPrefs.GetInt("TotalVials") + currentVials;
        Debug.Log("SAVE SCORE, TOTAL VIALS: " + totalVials);
        Debug.Log("SAVE SCORE, CURRENT VIALS: " + currentVials);

        PlayerPrefs.SetInt("TotalVials", totalVials);
        PlayerPrefs.SetInt("CantVials", currentVials);
        PlayerPrefs.Save();
        
        Debug.Log("TOTAL VIALS: " + PlayerPrefs.GetInt("TotalVials"));
        Debug.Log("CURRENTS VIALS: " + PlayerPrefs.GetInt("CantVials"));
    }

    public IEnumerator FlashRed()
    {
        Debug.Log("REDDDDDDDDDDDDDD");

        if (GameObject.Find("Player") != null)
        {
            spritePlayer.material.color = Color.red;

        }
       
        yield return new WaitForSeconds(0.5f);

        if (GameObject.Find("Player") != null)
        {
            spritePlayer.material.color = Color.white;

        }


    }
}
