using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] int fireBallDamage;

    void Start()
    {

    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        /*
        if (!collision.gameObject.tag.Equals("Enemy"))
        {
            Debug.Log("Non player hit");
            Destroy(gameObject);
        }
        */

        /*if (collision.gameObject.tag.Equals("Player"))
        {
            Debug.Log("Player hit");
            GameManager.Instance.playerDamaged(fireBallDamage);
            Destroy(gameObject);
        }

        if (collision.gameObject.tag.Equals("Ground"))
        {
            Destroy(gameObject);
        }*/
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            Debug.Log("Player hit");
            GameManager.Instance.playerDamaged(fireBallDamage);
            Destroy(gameObject);
        }

        if (collision.gameObject.tag.Equals("Ground"))
        {
            Destroy(gameObject);
        }
    }
}

