using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControler : MonoBehaviour
{
    [SerializeField] GameObject heart;
    [SerializeField] GameObject vial;
    public int enemyHealth;
    public int enemyDamage;
    [SerializeField] private ParticleSystem sysParticle;
    private bool drop = false;

    // Start is called before the first frame update
    void Start()
    {
        //sysParticle = GetComponent<ParticleSystem>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            GameManager.Instance.playerDamaged(enemyDamage);
            Destroy(gameObject);
        }

        if (collision.gameObject.tag.Equals("MainCamera"))
        {
            Destroy(gameObject);
        }
        Death();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            GameManager.Instance.playerDamaged(enemyDamage);
            Destroy(gameObject);
        }
        Death();
    }

    public void Death()
    {
        int randomDrop = Random.Range(0, 9);
        if (enemyHealth <= 0)
        {
            if (!sysParticle.isPlaying) sysParticle.Play();

            Debug.Log("random drop: " + randomDrop);
            gameObject.GetComponent<Renderer>().enabled = false;
            gameObject.GetComponent<Collider2D>().enabled = false;

            if (randomDrop == 2 && !drop)
            {
                GameObject heartObj = Instantiate(heart, new Vector3(transform.position.x, transform.position.y + 0.15f, 1), Quaternion.identity);
                drop = true;
            }
            else if (randomDrop == 6 && !drop)
            {

                GameObject vialObj = Instantiate(vial, new Vector3(transform.position.x, transform.position.y + 0.15f, 1), Quaternion.identity);
                drop = true;
            }

            Destroy(gameObject, sysParticle.main.duration);
        }
    }
}
