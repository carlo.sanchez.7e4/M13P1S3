using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControlerFly : EnemyControler
{
    private Transform target;
    [SerializeField] private float speed;
    private Vector3 range;
    private RaycastHit2D hit;
    private bool locked = false;
    private float step;
    [SerializeField] private float minRange;
    [SerializeField] private float maxRange;

    private void Start()
    {
        range = (Vector3.left - Vector3.up) * Random.Range(minRange, maxRange);

        if (GameObject.Find("Player") != null)
        {
            target = GameObject.Find("Player").GetComponent<Transform>();
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("Player") != null)
        {
            hit = Physics2D.Raycast(transform.position, range);
            Debug.DrawRay(transform.position, range, Color.green);
            target = GameObject.Find("Player").GetComponent<Transform>();
            float step = speed * Time.deltaTime;
            if (hit.collider != null)
            {
                //Debug.Log("He topat amb: " + hit.collider.gameObject.name);
            }
            if (locked)
            {
                //Un cop tinc el player seleccionat
                //Debug.Log("locked");
                transform.position = Vector3.MoveTowards(transform.position, target.position, step);
            }
            else
            {
                transform.position += Vector3.left * Time.deltaTime;
                if (hit.collider.gameObject.tag == "Player")
                {
                    // Debug.Log("hit");
                    locked = true;

                }

            }
        }
    }
}