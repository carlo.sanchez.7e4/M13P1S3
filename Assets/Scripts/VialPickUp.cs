using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VialPickUp : Consumables
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (PlayerPrefs.GetInt("VialMultiplier") == 1)
            {
                GameManager.Instance.currentVials += 2;

            }
            else {

                GameManager.Instance.currentVials++;

            }
            
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("MainCamera"))
        {
            Destroy(this.gameObject);
        }
    }
}
