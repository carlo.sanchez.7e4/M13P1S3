using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour
{
    [SerializeField] GameObject enemy;
    [SerializeField] GameObject boss;
    [SerializeField] GameObject bossHealthBar;

    private PlayerMovement pm;
    private bool onBossFight = false;
    public float panSpeed;


    // Start is called before the first frame update
    void Start()
    {
        pm = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        panSpeed = pm.maxSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        Spawner();
        if (!onBossFight) {
            SpawnBoss();
        }
    }

    void Spawner() {
        if (!onBossFight) {
            if (Random.Range(0, 1150) == 2) {
                GameObject obj = Instantiate(enemy, new Vector3(transform.position.x, transform.position.y, transform.position.z+1), Quaternion.identity);
            }
        }
    }

    void SpawnBoss() {
        if (GameManager.Instance.meters > 106) {
            Debug.Log("50 meters");
            //GameObject obj = Instantiate(boss, new Vector3(transform.position.x, transform.position.y, transform.position.z + 1), Quaternion.identity);
            boss.SetActive(true);
            bossHealthBar.SetActive(true);
            onBossFight = true;
        }
    }
}
